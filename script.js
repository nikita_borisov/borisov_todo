// Function for adding items to array with tasks

let taskArray = [],
body = 'body',
countTodo = 0,
currentPageContent = [],
checkBoxStatus = '',
checkBoxClass = '',
pageAmount;
const n = 5;

$(function () {
    let editText = '.edit_text',
    taskStatus = '.current-tasks',
    done = '#done',
    undone = '#not_done',
    all = '#counter';
// Function for entering text and adding new task
    function addingTasks(){
        const text = $('#input-text').val().trim();
        if (text.length) {
            let item = {
                text: text,
                checked: false,
                id: countTodo,
            };
            countTodo++;
            taskArray.push(item);
            counting();
            pagination(taskArray);
        }
        else {
            alert('You have entered nothing! Try again!');
        }
        $('.form__input-text').val('');
    }
    $('#submit_event').click(function () {
        addingTasks()
    });
    $('.form__input-text').keyup(function () {
        if (event.keyCode === 13) {
            addingTasks();
        }
    });
// Checking if all tasks are completed
    function counting() {
        let doneArray = _.filter(taskArray, (item) => item.checked === true),
            undoneArray = _.reject(taskArray, (item) => item.checked === true);
        const check = doneArray.length === taskArray.length && doneArray.length > 0;
        $(all).html(taskArray.length);
        $(undone).html(undoneArray.length);
        $(done).html(doneArray.length);
        if (taskArray.length === 0 || doneArray.length === 0 || undoneArray.length === 0) {
            callPagination();
        }
        else if (undoneArray.length !== 0) {
            $('.counter').prop('checked', false);
        }
        $("#count").prop('checked', check);
    }

// Function that put ticket to checked task
    $(body).on("change", ".check", function () {
        let getId = $(this).parent().attr('id');
        const check = this.checked;
        // $('#' + getId).addClass('completed') : $('#' + getId).removeClass('completed');
        $('#' + getId)[check ? 'addClass' : 'removeClass']('completed');
        taskArray[getId].checked = check;
        counting()
    });
    function selectingPage() {
        let getIdPage = parseInt($('.active').attr('id')),
            currentArray = taskArray,
            doneArray = _.filter(taskArray, (item) => item.checked === true),
            undoneArray = _.reject(taskArray, (item) => item.checked === true),
            tasksActive = '#total-amount';
        if ($(taskStatus).attr('id') === 'completed') {
            currentArray = doneArray;
            tasksActive = '#completed';
        }
        else if ($(taskStatus).attr('id') === 'not-done') {
            currentArray = undoneArray;
            tasksActive = '#not-done';
        }
        pagination(currentArray, getIdPage, tasksActive);
    }

// Checking all tasks
    $(body).on("change", '.counter', function () {
        const check = $('#count').is(':checked');
        check ? _.map(taskArray, (item) => item.checked = true) && selectingPage() : _.map(taskArray, (item) => item.checked = false) && callPagination();
        counting()
    });
    $(body).on('dblclick', '.todo_text', function () {
        let getId = $(this).parent().attr('id');
        $(this).after('<input class="text_edited" type="submit">');
        taskArray[getId].text = $('#' + getId + ' > .todo_text').text();
        $(this).replaceWith(function () {
            return '<input class="edit_text" autofocus maxlength="22" placeholder="Do not enter more than 22 characters" type="text" value="' + taskArray[getId].text + '">';
        });
        $(editText).focus().bind('blur keyup', function (event) {
            if (event.type == 'blur' || event.keyCode == '13') {
                submittingText(this, this, '.text_edited');
            }
        });
    });
    function submittingText(target, text, button) {
        let getId = $(target).parent().attr('id');
        taskArray[getId].text = $(editText).val().trim();
        if ($(editText).length && taskArray[getId].text) {
            $(text).replaceWith(function () {
                return '<span class="todo_text">' + taskArray[getId].text + '</span>';
            });
            $(button).remove();
        }
        else alert('Nothing was entered! Please write something!');
    }

    $(body).on('click', '.text_edited', function () {
        submittingText(this, editText, this);
    });
    $(body).on('click', '.result', function () {
        let currentTasks = '#' + $(this).attr('id');
        $('.result').removeClass('current-tasks');
        $(currentTasks).addClass('current-tasks');
        callPagination(currentTasks)
    });
    $(this).on("click", '.delete', function () {
        let newId = 0,
            getId = parseInt($(this).parent().attr('id'));
        $(this).parent().remove();
        taskArray = _.reject(taskArray, (item) => item.id === getId);
        _.map(taskArray, (item) => item.id = newId++);
        selectingPage();
        counting();
    });
    function removeAll() {
        taskArray = [];
        counting();
        $('#count').prop('checked', false);
        countTodo = 0;
        $('.pagination > :not(li:first)').remove();
        $('.pagination > li:first').addClass('active');
        pagination(taskArray);
    }

    $('#delete-all').click(function () {
        removeAll();
    });
    function removeDone() {
        taskArray = _.reject(taskArray, (item) => item.checked === true);
        $('#count').prop('checked', false);
        countTodo = 0;
        _.map(taskArray, (item) => item.id = countTodo++);
        counting();
        callPagination();
    }

    $('#delete').click(function () {
        removeDone();
    });
    $(body).on("click", '.page-number', function () {
        let getId = parseInt($(this).attr('id')),
            tasks = $(taskStatus).attr('id');
        callPagination('#' + tasks, getId)
    });
    function callPagination(currentTasks = '#total-amount', page) {
        let doneArray = _.filter(taskArray, (item) => item.checked === true),
            undoneArray = _.reject(taskArray, (item) => item.checked === true),
            currentArray = taskArray;
        if (currentTasks == '#completed'){
            currentArray = doneArray;
        } else if (currentTasks == '#not-done'){
            currentArray = undoneArray;
        }
        pagination(currentArray, page, currentTasks)
    }
    function countPages (array, currentPage) {
        let pageList = '';
        pageAmount = Math.ceil(array.length / n);
        for (let i = 1; i <= pageAmount; i++) {
            pageList += '<li class="page-number" id="' + i + 'page">' + i + '</li>';
        }
        if (currentPage === undefined || currentPage > pageAmount) {
            currentPage = pageAmount;
        }
        $('.pagination').html(pageList);
        $('#' + currentPage + 'page').addClass('active');
    }
    function pagination(array, currentPage, currentTasks) {
        countPages(array, currentPage);
        let pageNum = parseInt($('.active').attr('id')),
            pageContent = array.slice(n * (pageNum - 1), n * pageNum);
        currentPageContent = [];
        _.each(pageContent, function (item) {
            item.checked === true ? (
                    checkBoxClass = ' completed',
                    checkBoxStatus = 'checked="checked"'
            ) : (
                    checkBoxClass = '',
                    checkBoxStatus = ''
            );
            let task = '<li id="' + item.id + '" class="todo_task' + checkBoxClass + '"><input type="checkbox" ' + checkBoxStatus + ' class="check" id="check' + item.id + '"><span class="todo_text" id="todo_text' + item.id + '">' + item.text + '</span><input type="submit" value="delete" class="delete"></li>';
            currentPageContent.push(task);
        });
        $('.main__content').html(currentPageContent);
    }
});
